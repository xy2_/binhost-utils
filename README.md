# This project has been abandoned, due to its redundancy. Feel free to fork if you see any use for it.
binhost-utils
=============
A utility for building and maintaining a binary package server for Gentoo Linux.

Binary packages in built inside chroot environments on a host machine, you can
create any number of environments with different settings.

Configuring the Environment(s)
------------------------------
An environment is created with binhost-setup and represents a chroot.

Located in conf/ of your binhost directory (by default /var/lib/binhost, can be changed with the -c flag) is a configuration directory for each chroot. This directory will get mounted onto /etc/portage/ when using the environment. 

Quickstart
----------
	binhost-setup -a <architecture> -n <tarball name>
	binhost-chroot <environment> -e "emerge [package]"
	binhost-sync <environnment> -r <target machine>

Installing and Updating Packages
--------------------------------

Serving Packages to Hosts
-------------------------

Examples
--------

Tips, Tricks and Troubleshooting
---------------------------------
